﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "123s";
            int key = 2;
            Cryptographer crypto = new Cryptographer(text, key);
            Console.WriteLine(crypto.Encode());
            Console.WriteLine(crypto.Decode() == crypto.Encode());
        
            Fib fib = new Fib();
            Console.WriteLine(fib.getNumber(50));
            Console.ReadKey();
        }
    }

    class Cryptographer
    {
        string text;
        int key;
        public Cryptographer(string text, int key)
        {
            this.text = text;
            this.key = key;
        }

        public string Encode()
        {
            return Crypto();
        }

        public string Decode()
        {
            return Crypto();
        }

        private string Crypto()
        {
            byte[] bytes = Encoding.ASCII.GetBytes(text);
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(Convert.ToChar(Convert.ToInt32(bytes[i]) ^ key));
            }

            return builder.ToString();
        }
    }

    class Fib
    {
        Dictionary<int, long> fibs;

        public Fib()
        {
            fibs = new Dictionary<int, long>();
            fibs.Add(1, 1);
            fibs.Add(2, 1);
        }

        public long getNumber(int number)
        {
            long output;
            if (fibs.ContainsKey(number))
            {
                fibs.TryGetValue(number, out output);
                return output;
            }

            fibs.Add(number, getNumber(number - 1) + getNumber(number - 2));
            fibs.TryGetValue(number, out output);
            return output;
        }
    }
}
